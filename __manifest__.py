# -*- coding: utf-8 -*-
# more info here : http://www.odoo.com/documentation/10.0/reference/module.html#reference-module-manifest
# the object related to the quickcontact is addons/ressource
{
    'name': "Quickcontact",
    'summary': """
        Allow the quick creation of contacts     
        """,
    'description': """
        Quickcontact helps you to quickly create personnal contact that shortcut the company obligation.
        The person is automaticaly a company. 
    """,
    'author': "Jean Tinguely Awais",
    'website': "http://www.t-servi.com",
    'category': 'Backend/La_Bonne_Combine',
    'version': '0.1',
    'depends': ['base','mail'],
    'data': [
        'views/all.xml',
        #'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
}

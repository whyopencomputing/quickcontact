# -*- coding: utf-8 -*-

from odoo import models, fields, api
from gdata.sites.data import Field
from pygments.lexer import _inherit
import logging

import urllib2
from xml.etree import ElementTree as etree

#from twilio.rest.lookups import TwilioLookupsClient

KEY_API_TEL_SEARCH_CH = "ad39d8a29bb4ee0e04d5be76472959c6"

_logger     = logging.getLogger(__name__)

class Quickcontact(models.Model):
    _name       = 'quickcontact.quickcontact'
    _inherit    = ['mail.thread']

class PhoneWizzard ( models.Model):
    _name           = 'quickcontact.phonewizzard'
    _description    = 'search somebody in the search.ch database'
    _inherit        = ['mail.thread']
    
    phone_to_search = fields.Char( translate = True                         )
    search_result   = fields.Text( compute = '_search_tel', store = True    )
    
    #@api.depends('phone_to_search')
    def _search_tel(self):
        _logger.info("Hello from QuickcontactPhoneWizzard quickcontact/models/models.py")
        self.search_result = 'nothing found!'
        
    @api.one   
    def do_phone_search(self):
        _logger.info("Hello from QuickcontactPhoneWizzard.do_phone_search() quickcontact/models/models.py")
        the_url = 'https://tel.search.ch/api/?was=%s&key=XXXXXXXXXX'%( self.phone_to_search )
        
        req = urllib2.Request(the_url)
        handle = urllib2.urlopen(req)
        the_page = handle.read()
        mydict = {}
        search_root = etree.fromstring( the_page )
        the_string = ""
        for x in search_root.iter():
            if x.tag.find( "{http://tel.search.ch/api/spec/result/1.0/}") >= 0 :
                key = x.tag.split("{http://tel.search.ch/api/spec/result/1.0/}")[1]
                val = x.text
                mydict[ key ] = val
                the_string += "%s : %s \r\n"%(key, val)
        self.search_result = "Search for %s  \r\n  --> found  \r\n  %s in search.ch !"%(self.phone_to_search, the_string)
        